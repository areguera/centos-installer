# centos-installer

This project is community effort to reproduce the CentOS installer artwork.  It
downloads the latest CentOS release and uses it as input to produce a
customized iso file as output with the artwork provided by centos-logos
repository.

You can use this to create a virtual machine and test how the installer looks
like on different resolutions.  For example, you can create a virtual machine
in VirtualBox using the customized iso and when the system presents the inicial
selector, you select the Install option, press Tab to write `vga=ask`, then
press `Enter` to see the list of available resolutions you can use test the
installer on.

![CentOS Installer Welcome Screen](screenshots/centos-stream-9_27_11_2021_14_13_28.png "CentOS Installer Welcome Screen")

![CentOS Installer Available Resolutions](screenshots/centos-stream-9_27_11_2021_14_13_59.png "CentOS Installer Available Resolutions")

![CentOS Installer Summary](screenshots/centos-stream-9_27_11_2021_14_16_16.png "CentOS Installer Summary")

![CentOS Installer Time Zone](screenshots/centos-stream-9_27_11_2021_14_17_34.png "CentOS Installer Time Zone")


The CentOS installer artwork is dynamically created from the
centos-logos/backgrounds/ files. If you want to change the installer artwork,
please see the centos-logos/Makefile.
