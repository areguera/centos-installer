###############################################################################
# Rebuild latest iso to test anaconda artwork 
# Copyright (C) 2021 Alain Reguera Delgado
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
# 
# You should have received a copy of the GNU General Public License along with
# this program.  If not, see <https://www.gnu.org/licenses/>.
###############################################################################

# References:
# - https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html-single/customizing_anaconda/index#customizing-graphical-elements_branding-and-chroming-the-graphical-user-interface

major_release=9
minor_release=latest
release=$(major_release)-$(minor_release)
label=CentOS-Stream-$(major_release)-BaseOS-x86_64
anaconda_themedir=../../Packages/centos-logos/anaconda/theme

all: $(label)-customized.iso

CentOS-Stream-$(major_release)-latest-x86_64-dvd1.iso:
	wget http://mirror.stream.centos.org/$(major_release)-stream/BaseOS/x86_64/iso/CentOS-Stream-$(major_release)-$(minor_release)-x86_64-dvd1.iso

$(label)-customized.iso: CentOS-Stream-$(major_release)-latest-x86_64-dvd1.iso
	test -d $(label)-tmp || mkdir $(label)-tmp
	mount -o loop CentOS-Stream-$(major_release)-latest-x86_64-dvd1.iso $(label)-tmp
	cp -r $(label)-tmp $(label)
	umount $(label)-tmp
	cp $(label)/images/install.img $(label)-install.img
	unsquashfs $(label)-install.img
	mount -o sync squashfs-root/LiveOS/rootfs.img $(label)-tmp
	cp $(anaconda_themedir)/*.png $(label)-tmp/usr/share/anaconda/pixmaps/
	cp $(anaconda_themedir)/redhat.css $(label)-tmp/usr/share/anaconda/anaconda-gtk.css
	umount $(label)-tmp
	mksquashfs squashfs-root $(label)-install.img.customized
	cp $(label)-install.img.customized $(label)/images/install.img
	cd $(label); \
		genisoimage -U -r -v -T -J -joliet-long -V "$(label)" -volset "$(label)" -A "$(label)" -b isolinux/isolinux.bin -c isolinux/boot.cat -no-emul-boot -boot-load-size 4 -boot-info-table -eltorito-alt-boot -e images/efiboot.img -no-emul-boot -o ../$(label)-customized.iso .
	chown $${USER}:$${USER} $(label)-customized.iso

.PHONY: clean
clean:
	rm -r $(label)-tmp
	rm -r $(label)
	rm -r squashfs-root
	rm $(label)-install.img
	rm $(label)-install.img.customized
	rm $(label)-customized.iso
